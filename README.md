<!--
SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# YubiKey Management

Configuration of Yubikey devices, via their "management" application.

Allows reading of a YubiKey device's configuration, as well as
enabling and disabling of applications on a device (separately for the
USB and NFC interface).

E.g., the OTP application can be disabled for the USB interface.

*This crate is in an early development stage, and still very incomplete.*

## Context

This crate is a companion for the `openpgp-card` client library project
(https://gitlab.com/openpgp-card/openpgp-card).
In particular, it requires an
[`openpgp_card::CardBackend`](https://docs.rs/openpgp-card/latest/openpgp_card/trait.CardBackend.html)
implementation to interact with cards
