// SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

use crate::Command;

const INS_READ_CONFIG: u8 = 0x1D;
const INS_WRITE_CONFIG: u8 = 0x1C;
const INS_SET_MODE: u8 = 0x16;

const P1_DEVICE_CONFIG: u8 = 0x11;

pub(crate) fn select(aid: Vec<u8>) -> Command {
    Command::new(0x00, 0xA4, 0x04, 0x00, aid)
}

pub(crate) fn read_config() -> Command {
    Command::new(0x00, INS_READ_CONFIG, 0x00, 0x00, vec![])
}

pub(crate) fn write_config(aid: Vec<u8>) -> Command {
    Command::new(0x00, INS_WRITE_CONFIG, 0x00, 0x00, aid)
}

pub(crate) fn set_mode_device_config(aid: Vec<u8>) -> Command {
    Command::new(0x00, INS_SET_MODE, P1_DEVICE_CONFIG, 0x00, aid)
}
