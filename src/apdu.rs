// SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

use card_backend::SmartcardError;

#[derive(Clone, Copy)]
pub(crate) enum Expect {
    Empty,
    Some,
    // Short(u8),
}

#[derive(Clone, Debug)]
pub(crate) struct Command {
    // Class byte (CLA)
    cla: u8,

    // Instruction byte (INS)
    ins: u8,

    // Parameter bytes (P1/P2)
    p1: u8,
    p2: u8,

    // NOTE: data must be smaller than 64 kbyte
    data: Vec<u8>,
}

impl Command {
    /// Create an APDU Command.
    ///
    /// `data` must be smaller than 64 kbyte. If a larger `data` is passed,
    /// this fn will panic.
    pub fn new(cla: u8, ins: u8, p1: u8, p2: u8, data: Vec<u8>) -> Self {
        // This constructor is the only place `data` gets set, so it's
        // sufficient to check it here.
        if data.len() > u16::MAX as usize {
            panic!("'data' too big, must be <64 kbyte");
        }

        Command {
            cla,
            ins,
            p1,
            p2,
            data,
        }
    }

    /// Serialize a Command (for sending to a card).
    ///
    /// See OpenPGP card spec, chapter 7 (pg 47)
    pub(crate) fn serialize(
        &self,
        ext_len: bool,
        expect_response: Expect,
    ) -> Result<Vec<u8>, SmartcardError> {
        // FIXME? (from scd/apdu.c):
        //  T=0 does not allow the use of Lc together with Le;
        //  thus disable Le in this case.

        // "number of bytes in the command data field"
        assert!(self.data.len() <= u16::MAX as usize);
        let nc = self.data.len() as u16;

        let mut buf = vec![self.cla, self.ins, self.p1, self.p2];
        buf.extend(Self::make_lc(nc, ext_len)?);
        buf.extend(&self.data);
        buf.extend(Self::make_le(nc, ext_len, expect_response));

        Ok(buf)
    }

    /// Encode len for Lc field
    fn make_lc(len: u16, ext_len: bool) -> Result<Vec<u8>, SmartcardError> {
        if !ext_len && len > 0xff {
            return Err(SmartcardError::Error(format!(
                "Command len = {len:x?}, but extended length is unsupported by backend"
            )));
        }

        if len == 0 {
            Ok(vec![])
        } else if !ext_len {
            Ok(vec![len as u8])
        } else {
            Ok(vec![0, (len >> 8) as u8, (len & 255) as u8])
        }
    }

    /// Encode value for Le field
    /// ("maximum number of bytes expected in the response data field").
    fn make_le(nc: u16, ext_len: bool, expect_response: Expect) -> Vec<u8> {
        match (ext_len, expect_response) {
            (_, Expect::Empty) => {
                // No response data expected.
                // "If the Le field is absent, then Ne is zero"
                vec![]
            }
            (false, Expect::Some) => {
                // A short Le field consists of one byte with any value
                vec![0]
            }
            // (false, Expect::Short(size)) => {
            //     // A short Le field consists of one byte with any value
            //     vec![size]
            // }
            (true, Expect::Some) => {
                if nc == 0 {
                    // "three bytes (one byte set to '00' followed by two
                    // bytes with any value) if the Lc field is absent"
                    vec![0, 0, 0]
                } else {
                    // "two bytes (with any value) if an extended Lc field
                    // is present"
                    vec![0, 0]
                }
            } /* (true, Expect::Short(_i)) => {
               *     todo!("Unimplemented")
               * } */
        }
    }
}

#[allow(unused)]
#[derive(Clone, Debug)]
pub(crate) struct RawResponse {
    data: Vec<u8>,
    status: (u8, u8),
}

#[derive(Debug)]
pub(crate) struct Response {
    data: Vec<u8>,
}

impl AsRef<[u8]> for Response {
    fn as_ref(&self) -> &[u8] {
        &self.data
    }
}

impl TryFrom<RawResponse> for Response {
    type Error = SmartcardError;

    fn try_from(value: RawResponse) -> Result<Self, Self::Error> {
        if value.status == (0x90, 0x00) {
            Ok(Response { data: value.data })
        } else {
            Err(SmartcardError::SmartCardStatus(
                value.status.0,
                value.status.1,
            ))
        }
    }
}

impl TryFrom<Vec<u8>> for RawResponse {
    type Error = SmartcardError;

    fn try_from(mut data: Vec<u8>) -> Result<Self, Self::Error> {
        let sw2 = data.pop().ok_or(SmartcardError::Error(format!(
            "Unexpected response length {}",
            data.len()
        )))?;
        let sw1 = data.pop().ok_or(SmartcardError::Error(format!(
            "Unexpected response length {}",
            data.len()
        )))?;

        let status = (sw1, sw2);

        Ok(RawResponse { data, status })
    }
}
