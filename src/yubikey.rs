// SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

use std::collections::{HashMap, HashSet, LinkedList};
use std::fmt::{Display, Formatter};

use card_backend::SmartcardError;
use iso7816_tlv::simple::Tlv;
use regex::Regex;

pub(crate) const AID_MGR: &[u8] = &[0xA0, 0x00, 0x00, 0x05, 0x27, 0x47, 0x11, 0x17];
pub(crate) const AID_OTP: &[u8] = &[0xA0, 0x00, 0x00, 0x05, 0x27, 0x20, 0x01];

/// YubiKey firmware version
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Version {
    major: u8,
    minor: u8,
    patch: u8,
}

impl Version {
    pub fn major(&self) -> u8 {
        self.major
    }

    pub fn minor(&self) -> u8 {
        self.minor
    }

    pub fn patch(&self) -> u8 {
        self.patch
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}.{}.{}", self.major, self.minor, self.patch))
    }
}

const VERSION_STRING_PATTERN: &str = r"\b(?P<major>\d+).(?P<minor>\d).(?P<patch>\d)\b";

impl TryFrom<String> for Version {
    type Error = SmartcardError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let re = Regex::new(VERSION_STRING_PATTERN).unwrap();

        if let Some(caps) = re.captures(&value) {
            Ok(Version {
                major: caps[1].parse::<u8>().unwrap(),
                minor: caps[2].parse::<u8>().unwrap(),
                patch: caps[3].parse::<u8>().unwrap(),
            })
        } else {
            Err(SmartcardError::Error(format!(
                "Unexpected version string: '{}'",
                value
            )))
        }
    }
}

impl From<[u8; 3]> for Version {
    fn from(value: [u8; 3]) -> Self {
        Self {
            major: value[0],
            minor: value[1],
            patch: value[2],
        }
    }
}

/// Applications on a YubiKey (e.g. OTP, PIV, OpenPGP card, ...)
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Application {
    Otp,
    U2F,
    Openpgp,
    Piv,
    Oath,
    HsmAuth,
    Fido2,
}

impl From<Application> for u16 {
    fn from(value: Application) -> Self {
        match value {
            Application::Otp => 0x01,
            Application::U2F => 0x02,
            Application::Openpgp => 0x08,
            Application::Piv => 0x10,
            Application::Oath => 0x20,
            Application::HsmAuth => 0x100,
            Application::Fido2 => 0x200,
        }
    }
}

pub(crate) fn applications_to_bitmask(apps: HashSet<Application>) -> u16 {
    let mut bitmask = 0;
    apps.iter().for_each(|&a| bitmask |= u16::from(a));

    bitmask
}

fn applications(value: Option<u16>) -> LinkedList<Application> {
    let mut list = LinkedList::new();

    if let Some(value) = value {
        for a in [
            Application::Otp,
            Application::U2F,
            Application::Openpgp,
            Application::Piv,
            Application::Oath,
            Application::HsmAuth,
            Application::Fido2,
        ] {
            if value & u16::from(a) != 0 {
                list.push_back(a)
            }
        }
    }

    list
}

/// YubiKey form factor
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FormFactor {
    Unknown,
    UsbAKeychain,
    UsbANano,
    UsbCKeychain,
    UsbCNano,
    UsbCLightning,
    UsbABio,
    UsbCBio,
}

impl From<Option<u8>> for FormFactor {
    fn from(value: Option<u8>) -> Self {
        match value {
            Some(0x01) => FormFactor::UsbAKeychain,
            Some(0x02) => FormFactor::UsbANano,
            Some(0x03) => FormFactor::UsbCKeychain,
            Some(0x04) => FormFactor::UsbCNano,
            Some(0x05) => FormFactor::UsbCLightning,
            Some(0x06) => FormFactor::UsbABio,
            Some(0x07) => FormFactor::UsbCBio,
            _ => FormFactor::Unknown,
        }
    }
}

const TAG_USB_SUPPORTED: u8 = 0x01;
const TAG_SERIAL: u8 = 0x02;
pub(crate) const TAG_USB_ENABLED: u8 = 0x03;
const TAG_FORM_FACTOR: u8 = 0x04;
const TAG_VERSION: u8 = 0x05;
const TAG_AUTO_EJECT_TIMEOUT: u8 = 0x06;
const TAG_CHALRESP_TIMEOUT: u8 = 0x07;
const TAG_DEVICE_FLAGS: u8 = 0x08;
// const TAG_APP_VERSIONS: u8 = 0x09;
const TAG_CONFIG_LOCK: u8 = 0x0A;
// const TAG_UNLOCK: u8 = 0x0B;
// const TAG_REBOOT: u8 = 0x0C;
const TAG_NFC_SUPPORTED: u8 = 0x0D;
pub(crate) const TAG_NFC_ENABLED: u8 = 0x0E;

/// Metadata about a YubiKey device, as returned by [`YkManagement::read_config()`]
#[derive(Debug, PartialEq)]
pub struct DeviceInfo {
    usb_supported: LinkedList<Application>,
    serial: Option<u32>,
    usb_enabled: LinkedList<Application>,
    form_factor: FormFactor,
    version: Option<Version>,
    auto_eject_timeout: Option<u16>,
    chalresp_timeout: Option<u8>,
    device_flags: Option<u8>,
    // app_versions, // FIXME: what type of data is in that field?
    config_lock: Option<u8>,
    // unlock, // FIXME: what type of data is in that field?
    // reboot, // FIXME: what type of data is in that field?
    nfc_supported: LinkedList<Application>,
    nfc_enabled: LinkedList<Application>,
}

impl DeviceInfo {
    /// YubiKey serial, if available
    pub fn serial(&self) -> Option<u32> {
        self.serial
    }

    /// YubiKey version, if available
    pub fn version(&self) -> Option<Version> {
        self.version
    }

    /// YubiKey device form factor
    pub fn form_factor(&self) -> FormFactor {
        self.form_factor
    }

    /// Applications that are supported on the USB interface
    pub fn usb_supported(&self) -> HashSet<Application> {
        self.usb_supported.iter().cloned().collect()
    }

    /// Applications that are enabled on the USB interface
    pub fn usb_enabled(&self) -> HashSet<Application> {
        self.usb_enabled.iter().cloned().collect()
    }

    /// Applications that are supported on the NFC interface
    pub fn nfc_supported(&self) -> HashSet<Application> {
        self.nfc_supported.iter().cloned().collect()
    }

    /// Applications that are enabled on the NFC interface
    pub fn nfc_enabled(&self) -> HashSet<Application> {
        self.nfc_enabled.iter().cloned().collect()
    }
}

impl Display for DeviceInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(serial) = self.serial {
            f.write_fmt(format_args!("Serial {}\n", serial))?;
        }
        if let Some(version) = self.version {
            f.write_fmt(format_args!("Version {}\n", version))?;
        }
        f.write_fmt(format_args!("Form factor: {:?}\n", self.form_factor))?;

        f.write_fmt(format_args!("USB supported: {:?}\n", self.usb_supported))?;
        f.write_fmt(format_args!("USB enabled: {:?}\n", self.usb_enabled))?;
        f.write_fmt(format_args!("NFC supported: {:?}\n", self.nfc_supported))?;
        f.write_fmt(format_args!("NFC enabled: {:?}\n", self.nfc_enabled))?;

        if let Some(auto_eject_timeout) = self.auto_eject_timeout {
            f.write_fmt(format_args!(
                "Auto eject timeout: {:?}\n",
                auto_eject_timeout
            ))?;
        }

        if let Some(chalresp_timeout) = self.chalresp_timeout {
            f.write_fmt(format_args!("ChalResp timeout: {:?}\n", chalresp_timeout))?;
        }

        if let Some(device_flags) = self.device_flags {
            f.write_fmt(format_args!("Device flags: {:?}\n", device_flags))?;
        }

        if let Some(config_lock) = self.config_lock {
            f.write_fmt(format_args!("Config lock: {:?}\n", config_lock))?;
        }

        // app_versions ?
        // unlock ?
        // reboot ?

        Ok(())
    }
}

impl TryFrom<&[u8]> for DeviceInfo {
    type Error = SmartcardError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        // First byte is length
        assert_eq!(value[0] as usize, value.len() - 1);

        let mut map = HashMap::new();

        let mut remaining: &[u8] = &value[1..];
        while !remaining.is_empty() {
            let (r, rest) = Tlv::parse(remaining);
            remaining = rest;

            let tag: u8 = r.as_ref().unwrap().tag().into();
            let value = r.as_ref().unwrap().value().to_vec();

            map.insert(tag, value);
        }

        // println!("{:#x?}", map);

        fn to_applications(val: Option<&Vec<u8>>) -> LinkedList<Application> {
            applications(val.map(|x| match x.len() {
                1 => x[0] as u16,
                2 => u16::from_be_bytes([x[0], x[1]]),
                _ => unimplemented!(),
            }))
        }

        Ok(DeviceInfo {
            usb_supported: to_applications(map.get(&TAG_USB_SUPPORTED)),
            serial: map
                .get(&TAG_SERIAL)
                .map(|x| u32::from_be_bytes(x[0..4].try_into().unwrap())),
            usb_enabled: to_applications(map.get(&TAG_USB_ENABLED)),
            form_factor: map.get(&TAG_FORM_FACTOR).map(|x| x[0]).into(),
            version: map.get(&TAG_VERSION).map(|x| [x[0], x[1], x[2]].into()),
            auto_eject_timeout: map
                .get(&TAG_AUTO_EJECT_TIMEOUT)
                .map(|x| u16::from_be_bytes(x[0..2].try_into().unwrap())),
            chalresp_timeout: map.get(&TAG_CHALRESP_TIMEOUT).map(|x| x[0]),
            device_flags: map.get(&TAG_DEVICE_FLAGS).map(|x| x[0]),
            // app_versions: , // FIXME
            config_lock: map.get(&TAG_CONFIG_LOCK).map(|x| x[0]),
            // unlock: , // FIXME
            // reboot: , // FIXME
            nfc_supported: to_applications(map.get(&TAG_NFC_SUPPORTED)),
            nfc_enabled: to_applications(map.get(&TAG_NFC_ENABLED)),
        })
    }
}

#[cfg(test)]
mod test {
    use std::collections::LinkedList;

    use hex_literal::hex;

    use crate::yubikey::{DeviceInfo, FormFactor, Version};
    use crate::Application;

    #[test]
    fn test_config() {
        let config = hex!("2e0102023f0302023b020400f46eec04010105030502070602000007010f0801000d02023f0e02023b0a01000f0100");

        let di: DeviceInfo = (&config[..]).try_into().unwrap();

        let expected = DeviceInfo {
            serial: Some(16019180),
            version: Some(Version {
                major: 5,
                minor: 2,
                patch: 7,
            }),
            form_factor: FormFactor::UsbAKeychain,
            auto_eject_timeout: Some(0),
            chalresp_timeout: Some(15),
            device_flags: Some(0),
            config_lock: Some(0),

            usb_supported: LinkedList::from([
                Application::Otp,
                Application::U2F,
                Application::Openpgp,
                Application::Piv,
                Application::Oath,
                Application::Fido2,
            ]),
            usb_enabled: LinkedList::from([
                Application::Otp,
                Application::U2F,
                Application::Openpgp,
                Application::Piv,
                Application::Oath,
                Application::Fido2,
            ]),

            nfc_supported: LinkedList::from([
                Application::Otp,
                Application::U2F,
                Application::Openpgp,
                Application::Piv,
                Application::Oath,
                Application::Fido2,
            ]),
            nfc_enabled: LinkedList::from([
                Application::Otp,
                Application::U2F,
                Application::Openpgp,
                Application::Piv,
                Application::Oath,
                Application::Fido2,
            ]),
        };

        assert_eq!(di, expected);
    }
}
