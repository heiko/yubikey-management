// SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

//! Configuration of Yubikey devices, via their "management" application.
//! Allows enabling and disabling of applications on a YubiKey device,
//! separately for the USB and NFC interface.
//!
//! This crate is in an early development stage, and still very incomplete.
//!
//! See <https://github.com/Yubico/yubikey-manager/blob/main/yubikit/management.py>

mod apdu;
mod command;
mod yubikey;
mod yubikey4;

use std::collections::HashSet;

use card_backend::{CardBackend, CardTransaction, SmartcardError};

use crate::apdu::{Command, Expect, RawResponse, Response};
use crate::yubikey::{applications_to_bitmask, AID_MGR, AID_OTP, TAG_NFC_ENABLED, TAG_USB_ENABLED};
pub use crate::yubikey::{Application, DeviceInfo, FormFactor, Version};

const BUF_SIZE: usize = 256;

enum Interface {
    Usb,
    Nfc,
}

/// A YubiKey card, with the MANAGEMENT application selected
pub struct YkManagement {
    card: Box<dyn CardBackend + Sync + Send>,
    version: Version,
}

impl YkManagement {
    fn send(
        tx: &mut (dyn CardTransaction + Sync + Send),
        cmd: Command,
        response: bool,
    ) -> Result<Response, SmartcardError> {
        let exp = match response {
            true => Expect::Some,
            false => Expect::Empty,
        };

        let send = cmd.serialize(false, exp)?;

        log::trace!("send: {:x?}", send);

        let res = tx
            .transmit(&send, BUF_SIZE)
            .map_err(|e| SmartcardError::Error(format!("{e:?}")))?;

        log::trace!("received: {:x?}", res);

        let raw = RawResponse::try_from(res)?;
        raw.try_into()
    }

    /// Takes a `CardBackend` and turns it into a `YkManagement`, by calling
    /// `SELECT` on the management application of the card.
    ///
    /// (The YubiKey NEO is handled differently, and the OTP application
    /// is SELECTed)
    pub fn select(mut card: Box<dyn CardBackend + Sync + Send>) -> Result<Self, SmartcardError> {
        let version = {
            let mut tx = card.transaction(None)?;

            let cmd = command::select(AID_MGR.into());
            let resp = YkManagement::send(&mut *tx, cmd, true)?;

            let mut data = resp.as_ref();

            // YubiKey Edge incorrectly appends SW twice.
            if data[data.len() - 2..] == [0x90, 0x00] {
                // Drop the extra [0x90, 0x00]
                data = &data[0..data.len() - 2];
            }

            let s = String::from_utf8_lossy(data);
            let version = Version::try_from(s.to_string())?;

            // For YubiKey NEO, we use the OTP application for further commands
            if version.major() == 3 {
                // Workaround: "de-select" on NEO, otherwise it gets stuck.
                let _resp = YkManagement::send(
                    &mut *tx,
                    Command::new(0xa4, 0x04, 0x00, 0x08, vec![]),
                    false,
                );
                // UNCLEAR: This returns "6e00" (just like in ykman).
                // What is this command supposed to do?
                //
                // Without this command, the next "select" operation does
                // indeed fail. -> We ignore the Error in `_resp`.

                // Select OTP application
                let cmd = command::select(AID_OTP.into());
                let _resp = YkManagement::send(&mut *tx, cmd, true)?;

                // FIXME: get and use version from OTP application?
            }

            version
        };

        Ok(Self { card, version })
    }

    /// The YubiKey Version of the management application
    pub fn version(&self) -> Version {
        self.version
    }

    /// Reads the device config, using the 'INS_READ_CONFIG' command.
    /// Results are parsed into a `DeviceInfo` struct, but presented
    /// as returned by the card, without additional post-processing.
    pub fn read_config(&mut self) -> Result<DeviceInfo, SmartcardError> {
        log::debug!("read_config");

        // def read_device_info(self) -> DeviceInfo:
        //     require_version(self.version, (4, 1, 0))
        // return DeviceInfo.parse(self.backend.read_config(), self.version)

        let data = YkManagement::send(
            &mut *self.card.transaction(None)?,
            command::read_config(),
            true,
        )
        .map(move |r| r.as_ref().to_vec())?;

        DeviceInfo::try_from(data.as_slice())
    }

    /// Configure enabled applications, on YubiKey 4 devices
    pub(crate) fn set_mode(&mut self, apps: HashSet<Application>) -> Result<(), SmartcardError> {
        log::debug!("set_mode");

        if self.version.major() != 4 {
            unimplemented!();
        }

        let interface = yubikey4::usb_applications_to_interface(apps);
        log::trace!("  interface: {:?}", interface);

        let mode = yubikey4::interfaces_to_mode(interface);
        log::trace!("  mode: {}", mode);

        let cmd = command::set_mode_device_config(vec![mode, 0x00, 0x00, 0x00]);

        YkManagement::send(&mut *self.card.transaction(None)?, cmd, false)?;

        Ok(())
    }

    /// Configure enabled applications, on YubiKey 5 devices
    pub(crate) fn write_config(
        &mut self,
        apps: HashSet<Application>,
        iface: Interface,
    ) -> Result<(), SmartcardError> {
        log::debug!("write_config_usb");

        if self.version.major() != 5 {
            unimplemented!();
        }

        let bitmask: u16 = applications_to_bitmask(apps);
        let bitmask = bitmask.to_be_bytes();

        let cmd = command::write_config(vec![
            0x04, // len
            match iface {
                Interface::Usb => TAG_USB_ENABLED,
                Interface::Nfc => TAG_NFC_ENABLED,
            },
            0x02, // len
            bitmask[0],
            bitmask[1],
        ]);

        YkManagement::send(&mut *self.card.transaction(None)?, cmd, false)?;

        Ok(())
    }

    fn change_application_set(apps: &mut HashSet<Application>, change: &[Application], on: bool) {
        if on {
            // add apps to current configuration
            change.iter().for_each(|x| {
                apps.insert(*x);
            });
        } else {
            // remove apps from current configuration
            change.iter().for_each(|x| {
                apps.remove(x);
            });
        }
    }

    /// Change the set of enabled applications on the USB interface:
    /// If `on` is true, the applications in `change` are enabled.
    /// If `on` is false, the applications in `change` are disabled.
    ///
    /// This functionality is available for the YubiKey 4 and YubiKey 5.
    pub fn applications_change_usb(
        &mut self,
        change: &[Application],
        on: bool,
    ) -> Result<(), SmartcardError> {
        log::debug!("applications_change_usb, change: {:?}, on: {}", change, on);

        let cfg = self.read_config()?;

        let mut apps = cfg.usb_enabled();
        Self::change_application_set(&mut apps, change, on);

        match self.version.major() {
            5 => self.write_config(apps, Interface::Usb)?,
            4 => self.set_mode(apps)?,
            _ => unimplemented!(),
        };

        Ok(())
    }

    /// Change the set of enabled applications on the NFC interface:
    /// If `on` is true, the applications in `change` are enabled.
    /// If `on` is false, the applications in `change` are disabled.
    ///
    /// This functionality is available for the YubiKey 5.
    pub fn applications_change_nfc(
        &mut self,
        change: &[Application],
        on: bool,
    ) -> Result<(), SmartcardError> {
        log::debug!("applications_change_nfc, change: {:?}, on: {}", change, on);

        let cfg = self.read_config()?;

        let mut apps = cfg.nfc_enabled();
        Self::change_application_set(&mut apps, change, on);

        match self.version.major() {
            5 => self.write_config(apps, Interface::Nfc)?,
            _ => unimplemented!(),
        };

        Ok(())
    }
}
