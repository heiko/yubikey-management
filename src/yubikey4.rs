// SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

use std::collections::HashSet;

use crate::yubikey::Application;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub(crate) enum UsbInterface {
    Otp,  // 0x01
    Fido, // 0x02
    Ccid, // 0x04
}

pub(crate) fn usb_applications_to_interface(apps: HashSet<Application>) -> HashSet<UsbInterface> {
    let mut set = HashSet::new();

    if apps.contains(&Application::Otp) {
        set.insert(UsbInterface::Otp);
    }

    if apps.contains(&Application::U2F) || apps.contains(&Application::Fido2) {
        set.insert(UsbInterface::Fido);
    }

    if apps.contains(&Application::Oath)
        || apps.contains(&Application::Piv)
        || apps.contains(&Application::Openpgp)
        || apps.contains(&Application::HsmAuth)
    {
        set.insert(UsbInterface::Ccid);
    }

    set
}

pub(crate) fn interfaces_to_mode(ifaces: HashSet<UsbInterface>) -> u8 {
    // USB_INTERFACE.OTP,  # 0x00
    // USB_INTERFACE.CCID,  # 0x01
    // USB_INTERFACE.OTP | USB_INTERFACE.CCID,  # 0x02
    // USB_INTERFACE.FIDO,  # 0x03
    // USB_INTERFACE.OTP | USB_INTERFACE.FIDO,  # 0x04
    // USB_INTERFACE.FIDO | USB_INTERFACE.CCID,  # 0x05
    // USB_INTERFACE.OTP | USB_INTERFACE.FIDO | USB_INTERFACE.CCID,  # 0x06

    match (
        ifaces.contains(&UsbInterface::Otp),
        ifaces.contains(&UsbInterface::Ccid),
        ifaces.contains(&UsbInterface::Fido),
    ) {
        // otp, ccid, fido
        (true, false, false) => 0x00,
        (false, true, false) => 0x01,
        (true, true, false) => 0x02,
        (false, false, true) => 0x03,
        (true, false, true) => 0x04,
        (false, true, true) => 0x05,
        (true, true, true) => 0x06,

        (false, false, false) => panic!("This setting should not exist"),
    }
}
