// SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

use anyhow::Result;
use yubikey_management::YkManagement;

fn main() -> Result<()> {
    env_logger::init();

    let cards: Vec<_> = card_backend_pcsc::PcscBackend::card_backends(None)?.collect();

    println!("Found {} cards.", cards.len());
    println!();

    // This test code assumes that exactly one card is connected/found
    assert_eq!(cards.len(), 1);

    let card = cards.into_iter().next().unwrap();

    let mut ykm = YkManagement::select(card?)?;

    println!("YkManagement Version {}", ykm.version());
    println!();

    // ykm.applications_change_usb(&[yubikey_management::Application::Otp], false)?;
    // println!("OTP disabled");
    //
    // ykm.applications_change_usb(&[yubikey_management::Application::Otp], true)?;
    // println!("OTP enabled");

    let di = ykm.read_config()?;

    println!("DeviceInfo:\n{}", di);

    Ok(())
}
